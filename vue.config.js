module.exports = {
  configureWebpack: {
    output: {
      filename: "best_deals.js"
    },
    // No need for splitting
    optimization: {
      splitChunks: false
    }
  },
  // No need for css files all in js
  css: {
    extract: false
  },
  // No need a strange name of js files
  filenameHashing: false
};
